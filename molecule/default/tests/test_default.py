import os
import testinfra.utils.ansible_runner
import pytest
import time


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope="module", autouse=True)
def status(host):
    for i in range(200):
        cmd = host.run("curl --insecure --fail https://netbox-default/api/status")
        if cmd.rc == 0:
            return
        time.sleep(3)
    raise RuntimeError('Timed out waiting for application to start.')


def test_containers(host):
    with host.sudo():
        containers = host.docker.get_containers(name=["netbox", "netbox-worker", "netbox-redis", "netbox-redis-cache", "netbox-postgres"])
        for container in containers:
            assert container.is_running


def test_application_ui(host):
    cmd = host.run("curl --insecure --fail https://netbox-default/login/")
    assert cmd.rc == 0
    assert "<title>Home | NetBox</title>" in cmd.stdout
